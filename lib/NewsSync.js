'use strict';
var FeedParser = require('feedparser');
var request = require('request');
var async = require('async');
var FEEDS = require('../migrations/news_providers__init.json');

function parseSingleUrl (feed, process_batch_callback) {
    var req = request(feed['feed_url']);
    var feedparser = new FeedParser();

    req.on('error', function (error) {
        process_batch_callback(error);
    });

    req.on('response', function (res) {
        var stream = this;

        if (res.statusCode != 200) return this.emit('error', new Error('Bad status code'));

        stream.pipe(feedparser);
    });

    feedparser.on('error', function (error) {
        process_batch_callback(error);
    });

    var parsed_news = [];

    feedparser.on('readable', function () {
        // This is where the action is!
        var stream = this;
        var meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance
        var item;

        while (item = stream.read()) {
            var news_item = {
                pubdate: item.pubdate,
                title: item.title,
                summary: '', // item.summary, // storing summary is meaningless for data we receive
                origlink: item.origlink || item.link,
                author: item.author,
                provider: feed
            };
            parsed_news.push(news_item);
        }
    });

    feedparser.on('end', function () {
        process_batch_callback(null, parsed_news);
    })
}

function parseAll (process_batch_callback, callback_all_done) {
    callback_all_done = callback_all_done || function () {};
    process_batch_callback = process_batch_callback || function (err, data, callback) { callback(); };

    async.each(FEEDS,
        function (feed, callback) {
            parseSingleUrl(feed.data, function (err, parsed_news) {
                if (err) {
                    console.log(feed.data.name, 'import failed');
                    callback(err);
                }
                else {
                    process_batch_callback(parsed_news, function () {
                        console.log(feed.data.name, 'imported:', parsed_news ? parsed_news.length : 0, 'news added');
                        callback(null, feed.data.name);
                    });
                }
            })
        },
        function (err) {
            if (err) {
                console.log('import failed', err);
            }
            console.log('news import finished');
            callback_all_done.call(arguments);
        }
    );
}

module.exports = {parseAll: parseAll};