'use strict';
var request = require('request');
var API_BASE_URL = 'http://api.hitbtc.com/api/1/public/';

function api_request(path, cb) {
    request({url: API_BASE_URL + path, json: true}, cb);
}

function serialize(obj) {
    var str = [];
    for (var p in obj) {
        if (!obj.hasOwnProperty(p)) continue;
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
    return str.join("&");
}

module.exports = {

    getTime: function (cb) {
        api_request('time', function (error, response, data) {
                if (!error && response.statusCode == 200) {
                    cb(null, data.timestamp);
                } else cb(error, response, data);
            }
        );
    },

    getSymbol: function (currency, baseCurrency) {
        baseCurrency = baseCurrency || 'btc';
        return (baseCurrency + currency).toUpperCase();
    },

    getTicker: function (currency, cb) {
        api_request(this.getSymbol(currency) + '/ticker',
            function (error, response, data) {
                if (!error && response.statusCode == 200) {
                    cb(null, data);
                } else cb(error, response, data);
            }
        );
    },

    getOrderbook: function (currency, cb) {
        api_request(this.getSymbol(currency) + '/orderbook',
            function (error, response, data) {
                if (!error && response.statusCode == 200) {
                    cb(null, data);
                } else cb(error, response, data);
            }
        );
    },

    getTrades: function (currency, args, cb) {
        if (typeof args == 'function') {
            cb = args;
            args = {
                from: new Date().getTime() - 10000000,
                by: 'ts',
                format_wrap: 1,
                format_item: 'object'
            };
        }
        api_request(this.getSymbol(currency) + '/trades' + (args ? ('?' + serialize(args)) : ''),
            function (error, response, data) {
            if (!error && response.statusCode == 200) {
                cb(null, data);
            } else cb(error, response, data);
        });
    }
};