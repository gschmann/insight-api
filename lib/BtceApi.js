'use strict';
var request = require('request');
var API_BASE_URL = 'https://btc-e.com/api/2/';

function api_request(path, cb) {
    request({url: API_BASE_URL + path, json: true, rejectUnauthorized: false}, cb);
}

function serialize(obj) {
    var str = [];
    for (var p in obj) {
        if (!obj.hasOwnProperty(p)) continue;
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
    return str.join("&");
}

module.exports = {

    getSymbol: function (currency, baseCurrency) {
        baseCurrency = baseCurrency || 'btc';
        return baseCurrency + '_' + currency;
    },

    getTicker: function (currency, cb) {
        api_request(this.getSymbol(currency) + '/ticker',
            function (error, response, data) {
                if (!error && response.statusCode == 200) {
                    cb(null, data);
                } else cb(error, response, data);
            }
        );
    },

    getOrderbook: function (currency, cb) {
        api_request(this.getSymbol(currency) + '/depth',
            function (error, response, data) {
                if (!error && response.statusCode == 200) {
                    cb(null, data);
                } else cb(error, response, data);
            }
        );
    },

    getTrades: function (currency, args, cb) {
        if (typeof args == 'function') {
            cb = args;
            args = {
                // TODO: add default arguments
            };
        }
        api_request(this.getSymbol(currency) + '/trades' + (args ? ('?' + serialize(args)) : ''),
            function (error, response, data) {
            if (!error && response.statusCode == 200) {
                cb(null, data);
            } else cb(error, response, data);
        });
    }
};