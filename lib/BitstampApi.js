'use strict';
var request = require('request');
var API_BASE_URL = 'https://www.bitstamp.net/api/';

function api_request(path, cb) {
    request({url: API_BASE_URL + path, json: true}, cb);
}

function serialize(obj) {
    var str = [];
    for (var p in obj) {
        if (!obj.hasOwnProperty(p)) continue;
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
    return str.join("&");
}

module.exports = {

    getTicker: function (currency, cb) {
        api_request('ticker/',
            function (error, response, data) {
                if (!error && response.statusCode == 200) {
                    cb(null, data);
                } else cb(error, response, data);
            }
        );
    },

    getOrderbook: function (currency, cb) {
        api_request('order_book/',
            function (error, response, data) {
                if (!error && response.statusCode == 200) {
                    cb(null, data);
                } else cb(error, response, data);
            }
        );
    },

    getTrades: function (currency, args, cb) {
        if (typeof args == 'function') {
            cb = args;
            args = {
                // TODO: add default arguments
            };
        }
        args = args || {};
        if (!'time' in args) args['time'] = 'hour';
        api_request('transactions/?' + serialize(args),
            function (error, response, data) {
            if (!error && response.statusCode == 200) {
                cb(null, data);
            } else cb(error, response, data);
        });
    }
};