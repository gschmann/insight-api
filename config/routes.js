'use strict';

/**
 * Module dependencies.
 */
var config = require('./config');

module.exports = function (app) {

    var apiPrefix = config.apiPrefix;

    // Block routes
    var blocks = require('../app/controllers/blocks');
    app.get(apiPrefix + '/blocks', blocks.list);


    app.get(apiPrefix + '/block/:blockHash', blocks.show);
    app.param('blockHash', blocks.block);

    app.get(apiPrefix + '/block-index/:height', blocks.blockindex);
    app.param('height', blocks.blockindex);

    // Transaction routes
    var transactions = require('../app/controllers/transactions');
    app.get(apiPrefix + '/tx/:txid', transactions.show);
    app.param('txid', transactions.transaction);
    app.get(apiPrefix + '/txs', transactions.list);
    app.get(apiPrefix + '/txs/snapshot', transactions.snapshot);
    app.post(apiPrefix + '/tx/send', transactions.send);

    // Address routes
    var addresses = require('../app/controllers/addresses');
    app.get(apiPrefix + '/addr/:addr', addresses.show);
    app.get(apiPrefix + '/addr/:addr/utxo', addresses.utxo);
    app.get(apiPrefix + '/addrs/:addrs/utxo', addresses.multiutxo);
    app.post(apiPrefix + '/addrs/utxo', addresses.multiutxo);

    // Address property routes
    app.get(apiPrefix + '/addr/:addr/balance', addresses.balance);
    app.get(apiPrefix + '/addr/:addr/totalReceived', addresses.totalReceived);
    app.get(apiPrefix + '/addr/:addr/totalSent', addresses.totalSent);
    app.get(apiPrefix + '/addr/:addr/unconfirmedBalance', addresses.unconfirmedBalance);

    // Status route
    var st = require('../app/controllers/status');
    app.get(apiPrefix + '/status', st.show);

    app.get(apiPrefix + '/sync', st.sync);
    app.get(apiPrefix + '/peer', st.peer);

    // Currency
    var currency = require('../app/controllers/currency');
    app.get(apiPrefix + '/currency', currency.index);

    // News routes
    var news = require('../app/controllers/news');
    app.get(apiPrefix + '/news', news.list);
    app.get(apiPrefix + '/news/stats', news.stats);

    // Pools routes
    var pools = require('../app/controllers/pools');
    app.get(apiPrefix + '/pools/blocks_mined_per_pool', pools.blocks_mined_per_pool);

    // Block Headers routes
    var block_headers = require('../app/controllers/block_headers');
    app.get(apiPrefix + '/block-headers', block_headers.list);

    // Markets routes
    var markets = require('../app/controllers/markets');
    app.get(apiPrefix + '/markets/stats', markets.stats);
    app.get(apiPrefix + '/markets/candlesticks', markets.candlesticks);
    app.get(apiPrefix + '/markets/candlesticks/latest', markets.latest_candlesticks);
    app.get(apiPrefix + '/markets/orderbook', markets.orderbook);
    app.get(apiPrefix + '/markets/trades', markets.trades);
    app.get(apiPrefix + '/markets/ticker', markets.ticker);

    //Home route
    var index = require('../app/controllers/index');
    app.get(apiPrefix + '/version', index.version);
    app.get('*', index.render);
};
