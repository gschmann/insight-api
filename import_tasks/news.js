'use strict';
var models = require('../models');
var logger = require('../lib/logger').logger;
var NewsSync = require('../lib/NewsSync');
var async = require('async');

function sync_news_to_sql (cb) {
    var skipped = 0;
    var inserted = 0;
    NewsSync.parseAll(function (news, callback_outer) {
        async.each(news, function (ni, callback_inner) {
            ni.link = ni.link || ni.origlink;
            ni.NewsProviderId = ni.NewsProviderId || ni.provider.id;
            models.NewsItem
                .findOrCreate({where: {link: ni.link}, defaults: ni})
                .success(function (ni, created) {
                    if (created) {
                        logger.info('INSERTED', ni.link);
                        inserted++;
                    }
                    else {
//                        logger.info('skipped', ni.link);
                        skipped++;
                    }
                    callback_inner();
                });
        }, function (err) {
            callback_outer();
        });
    }, function (err) {
        cb(err, inserted, skipped);
    });
}

module.exports = function (cb) {
    cb = cb || function () {};
    logger.info('new import task for model news_item');

    models.ImportTask.create({model: 'news_item'}).success(function (import_task) {
        logger.info('import task created with id', import_task.id);

        sync_news_to_sql(function (err, imported, skipped) {
            if (err) {
                logger.info('sync_news_to_sql finished with error', err, ';',
                    imported, 'news items imported and', skipped, 'skipped');

            } else {
                logger.info('import_trades finished with', imported, 'trades imported and', skipped, 'skipped');
                import_task.completed = true;
            }
            import_task.imported_count = imported;
            import_task.ignored_count = skipped;
            import_task.save().success(function () {
                logger.info('import task', import_task.id, 'marked as completed');
                cb(null, import_task);
            }).failure(function (err) {
                logger.info('updating completeness status for import task', import_task.id, 'failed');
                cb(err);
            });
        });
    }).failure(function (err) {
        logger.info('unable to create import task for model "news_item"', err);
    })
};