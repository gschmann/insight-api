'use strict';
var models = require('../models');
var logger = require('../lib/logger').logger;
var async = require('async');

/*
 * One call for each market-currency pair
 */
function import_trades(market, currency, cb) {
    cb = cb || function (err, imported, skipped) {};
    switch (market) {
        case 'btce':
            import_btce(currency, cb);
            break;
        case 'bitstamp':
            import_bitstamp(currency, cb);
            break;
        case 'hitbtc':
            import_hitbtc(currency, cb);
            break;
        default:
            cb({'error': 'wrong market', details: {'market': market}}, 0, 0);
            return;
    }
}

function import_btce (currency, cb) {
    var api = require('../lib/BtceApi');
    var args = {}; // TODO: define args here
    api.getTrades(currency, args, function (err, trades) {
        if (err) {
            cb(err, 0, 0);
            return;
        }
        logger.info('trades recieved', trades.length);
        var inserted = 0;
        var skipped = 0;
        async.each(trades, function (trade, callback) {
            // TODO: handle trade types (trade_type: ask/bid in server response)
            models.Trade.findOrCreate({where: {
                market: 'btce',
                currency: currency,
                tid: trade.tid
            }, defaults: {
                market: 'btce',
                currency: currency,
                time: trade.date * 1000,
                tid: trade.tid,
                price: trade.price,
                amount: trade.amount
            }}).success(function (t, created) {
                if (created) {
//                    logger.info('INSERTED', t.tid, 'btce', currency);
                    inserted++;
                } else {
//                    logger.info('SKIPPED', t.tid);
                    skipped++;
                }
                callback();
            }).failure(function (err) {
                callback(err);
            });
        }, function all_done(err) {
            cb(err, inserted, skipped);
        });
    });
}

function import_bitstamp (currency, cb) {
    if (currency != 'usd') {
        cb({error: 'only usd currency is allowed for bitstamp market'}, 0, 0);
        return;
    }
    var api = require('../lib/BitstampApi');
    var args = {}; // TODO: define args here
    api.getTrades(currency, args, function (err, trades) {
        if (err) {
            cb(err, 0, 0);
            return;
        }
        logger.info('trades recieved', trades.length);
        var inserted = 0;
        var skipped = 0;
        async.each(trades, function (trade, callback) {
            models.Trade.findOrCreate({where: {
                market: 'bitstamp',
                currency: currency,
                tid: trade.tid
            }, defaults: {
                market: 'bitstamp',
                currency: currency,
                time: trade.date * 1000,
                tid: trade.tid,
                price: trade.price,
                amount: trade.amount
            }}).success(function (t, created) {
                if (created) {
//                    logger.info('INSERTED', t.tid, 'bitstamp', currency);
                    inserted++;
                } else {
//                    logger.info('SKIPPED', t.tid);
                    skipped++;
                }
                callback();
            }).failure(function (err) {
                callback(err);
            });
        }, function all_done(err) {
            cb(err, inserted, skipped);
        });
    });
}

function import_hitbtc (currency, cb) {
    var api = require('../lib/HitBtcApi');
    var args = {
        'from': 0,
        'by': 'ts',
        'format_wrap': false,
        'format_item': 'object'
    };
    // TODO: refactor extract Trade.getLastTimestamp(market, currency)
    async.series([
        function (cb1) {
            models.Trade.find({
                where: {
                    market: 'hitbtc',
                    currency: currency
                },
                order: 'time DESC',
                limit: 1
            }).success(function (last_trade) {
                if (last_trade && 'time' in last_trade) {
                    console.log(last_trade.time);
                    args.from = last_trade.time.getTime(); // TODO: figure out why it always ends with 000
                }
                cb1();
            }).failure(function (err) {
                cb1(err);
            })
        },
        function (cb1) {
            api.getTrades(currency, args, function (err, trades) {
                if (err) {
                    cb1(err, 0, 0);
                    return;
                }
                logger.info('trades recieved', trades.length);
                var inserted = 0;
                var skipped = 0;
                async.each(trades, function (trade, callback) {
                    models.Trade.findOrCreate({where: {
                        market: 'hitbtc',
                        currency: currency,
                        tid: trade.tid
                    }, defaults: {
                        market: 'hitbtc',
                        currency: currency,
                        time: trade.date,
                        tid: trade.tid,
                        price: trade.price,
                        amount: trade.amount
                    }}).success(function (t, created) {
                        if (created) {
//                            logger.info('INSERTED', t.tid, 'hitbtc', currency);
                            inserted++;
                        } else {
//                    logger.info('SKIPPED', t.tid);
                            skipped++;
                        }
                        callback();
                    }).failure(function (err) {
                        callback(err);
                    });
                }, function all_done(err) {
                    cb1(err, inserted, skipped);
                });
            });
        }],
        function (err, arr_of_calls) {
            if (arr_of_calls.length > 1) { // getting last timestamp didn't fall
                cb.call(null, err, arr_of_calls[1][0], arr_of_calls[1][1]);
            } else {
                cb.call(null, err, 0, 0);
            }
        }
    )

}

module.exports = function (market, currency, cb) {
    cb = cb || function () {};
    logger.info('new import task for model trade');

    models.ImportTask.create({model: 'trade:' + market + ':' + currency}).success(function (import_task) {
        logger.info('import task created with id', import_task.id);

        import_trades(market, currency, function (err, imported, skipped) {
            console.log('import_trades_callback', market, currency, arguments);
            if (err) {
                logger.info('import_trades finished with error', err, ';',
                    imported, 'trades imported and', skipped, 'skipped');

            } else {
                logger.info('import_trades(' + market + ', ' + currency +
                    ') finished with', imported, 'trades imported and', skipped, 'skipped');
                import_task.completed = true;
            }
            import_task.imported_count = imported;
            import_task.ignored_count = skipped;
            import_task.save().success(function () {
                logger.info('import task', import_task.id, 'marked as completed');
                cb(null, import_task);
            }).failure(function (err) {
                logger.info('updating completeness status for import task', import_task.id, 'failed');
                cb(err, import_task);
            });
        });
    }).failure(function (err) {
        logger.info('unable to create import task for model "trade"', err, arguments);
    })
};