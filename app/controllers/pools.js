'use strict';
var db = require('../../models');
var async = require('async');

exports.blocks_mined_per_pool = function (req, res) {
    var query = 'SELECT pool_name, count(*) as blocks_mined FROM "BlockHeaders" GROUP BY pool_name ORDER BY blocks_mined DESC;';
    db.sequelize.query(query)
        .success(function (pool_data) {
            res.jsonp({pool_data: pool_data});
        })
        .failure(function (err) {
            res.status(500).send(err);
        }
    );
};