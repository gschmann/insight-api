'use strict';
var models = require('../../models');
var async = require('async');

// TODO: refactor extract (see markets.js for the same method)
function prepare_where_params (input, model) {
    // This function is a draft of a django-style to sequlize-style query filtering params translator
    var result = {};
    for (var key in input) {
        if (!input.hasOwnProperty(key)) continue;
        var splitted = key.split('__');
        // skip if key is not a provided model existing field
        if (!model.tableAttributes.hasOwnProperty(splitted[0])) continue;
        if (splitted.length == 1) {
            result[key] = input[key];
        }
        if (splitted.length == 2) {
            // it's a case for translating django-style id__gte=15 to sequlize-understandable id: {gte: 15}
            result[splitted[0]] = result[splitted[0]] || {}; //init dict if not present yet
            result[splitted[0]][splitted[1]] = ((splitted[1] == 'like') ? input[key] : +input[key]);
            // TODO: actually it seems that sequilize 1.7 deals with numerical stuff only;
        }
        else {
            // TODO: manage more complex cases like customer__manager__id__gte=2
        }
    }
    return result;
}

exports.list = function (req, res) {
    var page = +req.query.page || 1;
    var per_page = +req.query.per_page || 20;
    models.BlockHeader.findAndCountAll({
        order: 'height DESC',
        limit: per_page,
        offset: (page - 1) * per_page,
        where: prepare_where_params(req.query, models.BlockHeader) || {}
    }).success(function (result) {
        res.jsonp({
            block_headers: result.rows,
            length: result.rows.length,
            total: result.count,
            page: page,
            per_page: per_page,
            total_pages: Math.ceil(result.count / per_page)
        });
    }).error(function (err) {
        res.status(500).send(err);
    });
};