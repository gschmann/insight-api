'use strict';
var models = require('../../models');
var async = require('async');

var markets_available = { // TODO: refactor
    hitbtc: {
        currencies: ['usd', 'eur'],
        api: require('../../lib/HitBtcApi')
    },
    btce: {
        currencies: ['usd', 'eur'],
        api: require('../../lib/BtceApi')
    },
    bitstamp: {
        currencies: ['usd'],
        api: require('../../lib/BitstampApi')
    }
};

exports.stats = function (req, res) {
    // TODO: use async
    var result = {};
    async.parallel([
        function (cb) {
            models.Candlestick.count().success(function (candlestick_count) {
                result.candlestick_count = candlestick_count;
                cb();
            }).failure(function (err) { cb(err); });
        },
        function (cb) {
            models.Trade.count().success(function (trade_count) {
                result.trade_count = trade_count;
                cb();
            }).failure(function (err) { cb(err); });
        },
        function (cb) {
            models.ImportTask.count({where: {model: {like: 'trade%'}, completed: false}}).success(function (incomplete_import_tasks_count) {
                result.incomplete_import_tasks_count = incomplete_import_tasks_count;
                cb();
            }).failure(function (err) { cb(err); });
        },
        function (cb) {
            models.ImportTask.findAll({where: {model: {like: 'trade%'}, completed: false}}).success(function (incomplete_import_tasks) {
                result.incomplete_import_tasks = incomplete_import_tasks;
                cb();
            }).failure(function (err) { cb(err); });
        },
        function (cb) {
            models.ImportTask.find({where: {model: {like: 'trade%'}}, limit: 1, order: 'id desc'}).success(function (import_tasks) {
                result.latest_import_task = (import_tasks && import_tasks.dataValues) ? import_tasks.dataValues : null;
                cb();
            }).failure(function (err) { cb(err); });
        }
    ], function (err, cb) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.jsonp(result);
        }

    });
};

// TODO: refactor extract (see block_headers.js for the same method)
function prepare_where_params (input, model) {
    // This function is a draft of a django-style to sequlize-style query filtering params translator
    var result = {};
    for (var key in input) {
        if (!input.hasOwnProperty(key)) continue;
        var splitted = key.split('__');
        // skip if key is not a provided model existing field
        if (!model.tableAttributes.hasOwnProperty(splitted[0])) continue;
        if (splitted.length == 1) {
            result[key] = input[key];
        }
        if (splitted.length == 2) {
            // it's a case for translating django-style id__gte=15 to sequlize-understandable id: {gte: 15}
            result[splitted[0]] = result[splitted[0]] || {}; //init dict if not present yet
            result[splitted[0]][splitted[1]] = ((splitted[1] == 'like') ? input[key] : +input[key]);
            // TODO: actually it seems that sequilize 1.7 deals with numerical stuff only;
        }
        else {
            // TODO: manage more complex cases like customer__manager__id__gte=2
        }
    }
    return result;
}

function prepare_for_csv (rows) {
    var result = [['open', 'close', 'min', 'max', 'volume', 'currency volume', 'trade count', 'time']];
    for (var i = 0; i < rows.length; i ++) {
        result.push([rows[i].open, rows[i].close, rows[i].min, rows[i].max, rows[i].volume, rows[i].currency_volume, rows[i].count, rows[i].time]);
    }
    return result;
}

exports.candlesticks = function (req, res) {
    var page = parseInt(req.query.page) || 1;
    var per_page = parseInt(req.query.per_page) || 10;

    var query_dict = {
        limit: per_page,
        offset: per_page * (page - 1),
        order: 'time ASC',
        where: prepare_where_params(req.query, models.Candlestick) || {}
    };

    models.Candlestick.findAndCountAll(query_dict).success(function (result) {
        result.limit = query_dict.limit;
        result.row_count = result.rows.length;
        result.offset = query_dict.offset;
        result.page = page;
        result.per_page = per_page;
        result.total_pages = Math.ceil(result.count / result.per_page);
        if ('format' in req.query && req.query.format == 'csv') {
            var csv = require('express-csv');
            res.csv(prepare_for_csv(result.rows));
        } else {
            res.jsonp(result);
        }
    }).failure(function (err) {
        res.status(500).send(err);
    });
};

exports.latest_candlesticks = function (req, res) {
    /*
     -- example query
     WITH t as (
         SELECT
            -- market, currency, period,
            time, open, close, price, min, max, volume, currency_volume, count
         FROM "Candlesticks"
         WHERE
            market = 'hitbtc' AND
            currency = 'usd' AND
            period = 'minute'
         ORDER BY time desc limit 10
     ) SELECT * from t ORDER BY time;
     */
    var limit = req.query.limit || 10000;
    var offset = req.query.offset || 0;
    var market = (req.query.market && req.query.market in markets_available) ? req.query.market : 'hitbtc';
    var currency = (req.query.currency && markets_available[market].currencies.indexOf(req.query.currency) != -1) ?
        req.query.currency : 'usd';
    var period = req.query.period || 'day';
    var query = "WITH t as (SELECT time, open, close, price, min, max, volume, currency_volume, count " +
        "FROM \"Candlesticks\"" +
        "WHERE market = '" + market + "' AND currency = '" + currency + "' AND period = '" + period +
        "' ORDER BY time desc limit " + limit + "offset " + offset + ")" +
        "SELECT * from t ORDER BY time;";
    models.sequelize.query(query)
        .success(function (rows) {
            if ('format' in req.query && req.query.format == 'csv') {
                var csv = require('express-csv');
                res.csv(prepare_for_csv(rows));
            } else {
                res.jsonp({
                    rows: rows,
                    row_count: rows.length,
                    market: market,
                    currency: currency,
                    limit: limit,
                    period: period
                });
            }
        })
        .failure(function (err) {
            res.status(500).send(err);
        }
    );
};

function groupOrderbookData(data, price_step) {
    var result = {bids: [], asks: []};
    var buffer = {};
    var len = data.asks ? data.asks.length : 0;
    
    for (var i = 0; i < len; i ++) {

        var ask = +data.asks[i][0],
            size = +data.asks[i][1];
        // with ask = 354.62 and pg = 0.1, ask_floored = 354.6
        var ask_floored = (Math.floor(ask / price_step) * price_step).toFixed(2);
        if (!(ask_floored in buffer)) {
            buffer[ask_floored] = 0;
        }
        buffer[ask_floored] += size;
    }

    for (ask in buffer) {
        if (!buffer.hasOwnProperty(ask))
            continue;
        result.asks.push([+ask, buffer[ask]]);
    }

    buffer = {};

    len = data.asks ? data.bids.length : 0;

    for (i = 0; i < len; i ++) {
        var bid = +data.bids[i][0];
        size = +data.bids[i][1];
        // with bid = 354.62 and pg = 0.1, bid_floored = 354.6
        var bid_floored = (Math.floor(bid / price_step) * price_step).toFixed(2);
        if (!(bid_floored in buffer)) {
            buffer[bid_floored] = 0;
        }
        buffer[bid_floored] += size;
    }
    for (bid in buffer) {
        if (!buffer.hasOwnProperty(bid))
            continue;
        result.bids.push([+bid, buffer[bid]]);
    }
    return result;
}

function normalizeOrderbook(market, data, limit) {
    var result = {};
    if (market == 'hitbtc' || market == 'btce' || market == 'bitstamp') {
        result['asks'] = [];
        if (data && data.asks) {
            for (var i = 0; i < Math.min(data.asks.length, limit); i ++) {
                result['asks'].push({sum: data.asks[i][0] * data.asks[i][1], ask: data.asks[i][0], size: data.asks[i][1]})
            }
        }
        result['bids'] = [];
        if (data && data.bids) {
            for (i = 0; i < Math.min(data.bids.length, limit); i ++) {
                result['bids'].push({sum: data.bids[i][0] * data.bids[i][1], bid: data.bids[i][0], size: data.bids[i][1]})
            }
        }
    } else {
        return data;
    }
    return result;
}

function normalizeTrades(market, data, limit) {
    var result = [];
    if (market == 'hitbtc') {
        if (data && data.length) {
            for (var i = 0; i < Math.min(data.length, limit); i ++) {
                result.push({
                    time: data[i].date,
                    price: data[i].price,
                    volume: data[i].amount
                })
            }
        }
    } else if (market == 'btce') {
        if (data && data.length) {
            for (i = 0; i < Math.min(data.length, limit); i ++) {
                result.push({
                    time: data[i].date * 1000,
                    price: data[i].price,
                    volume: data[i].amount
                })
            }
        }
    } else if (market == 'bitstamp') {
        if (data && data.length) {
            for (i = 0; i < Math.min(data.length, limit); i ++) {
                result.push({
                    time: data[i].date * 1000,
                    price: +data[i].price,
                    volume: +data[i].amount
                })
            }
        }
    } else {
        return data;
    }
    return result;
}

function normalizeTicker(market, data) {
    var result = {};
    // what to do with volume ?
    // what to do with
    if (market == 'hitbtc') {
        // ask, bid, open, last, low, high, timestamp, volume, volume_quote (?)
        // avg ?
        result.ask = +data.ask;
        result.bid = +data.bid;
        result.high = +data.high;
        result.low = +data.low;
        result.last = +data.last;
        result.timestamp = data.timestamp;
    } else if (market == 'btce') {
        result.ask = +data.ticker.sell;
        result.bid = +data.ticker.buy;
        result.high = +data.ticker.high;
        result.low = +data.ticker.low;
        // no open ?
        result.last = +data.ticker.last;
        result.timestamp = data.ticker.updated * 1000;
    } else if (market == 'bitstamp') {
        result.ask = +data.ask;
        result.bid = +data.bid;
        result.high = +data.high;
        result.low = +data.low;
        result.last = +data.last;
        result.timestamp = data.timestamp * 1000;
    } else {
        return data;
    }
    return result;
}

exports.orderbook = function (req, res) {
    // TODO: refactor:extract
    if (req.query.market && req.query.market in markets_available) {
        var market = req.query.market;
    } else {
        res.status(500).send({error: 'wrong or empty market parameter specified'});
        return;
    }
    if (req.query.currency && markets_available[market].currencies.indexOf(req.query.currency) != -1) {
        var currency = req.query.currency;
    } else {
        res.status(500).send({error: 'empty or incorrect (for market ' + market + ') currency parameter specified'});
        return;
    }

    var api = markets_available[market].api;

    api.getOrderbook(currency, function (error, data) {
        if (error) {
            res.status(500).send(error);
        } else {
            res.jsonp(normalizeOrderbook(market, groupOrderbookData(data, req.query.grouping_price_step || 0.01), req.query.limit || 10));
        }
    });
};

exports.trades = function (req, res) {
    // TODO: refactor:extract
    if (req.query.market && req.query.market in markets_available) {
        var market = req.query.market;
    } else {
        res.status(500).send({error: 'wrong or empty market parameter specified'});
        return;
    }
    if (req.query.currency && markets_available[market].currencies.indexOf(req.query.currency) != -1) {
        var currency = req.query.currency;
    } else {
        res.status(500).send({error: 'empty or incorrect (for market ' + market + ') currency parameter specified'});
        return;
    }

    var api = markets_available[market].api;

    api.getTrades(currency, function (error, data) {
        if (error) {
            res.status(500).send(error);
        } else {
            res.jsonp({trades: normalizeTrades(market, data, req.query.limit || 10)});
        }
    });
};

exports.ticker  = function (req, res) {
    // TODO: refactor:extract
    if (req.query.market && req.query.market in markets_available) {
        var market = req.query.market;
    } else {
        res.status(500).send({error: 'wrong or empty market parameter specified'});
        return;
    }
    if (req.query.currency && markets_available[market].currencies.indexOf(req.query.currency) != -1) {
        var currency = req.query.currency;
    } else {
        res.status(500).send({error: 'empty or incorrect (for market ' + market + ') currency parameter specified'});
        return;
    }

    var api = markets_available[market].api;

    api.getTicker(currency, function (error, data) {
        if (error) {
            res.status(500).send(error);
        } else {
            res.jsonp({ticker: normalizeTicker(market, data)});
        }
    });
};