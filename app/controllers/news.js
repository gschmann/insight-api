'use strict';
var models = require('../../models');
var async = require('async');

exports.list = function (req, res) {
    models.NewsItem.findAll({order: 'pubdate DESC', limit: 40, include: [models.NewsProvider]}).success(function (news) {
        res.jsonp({
            news: news,
            length: news.length
        });
    }).error(function (err) {
        res.status(500).send(err);
    });
};

exports.stats = function (req, res) {
    var result = {};
    async.parallel([
        function (cb) {
            models.NewsProvider.count().success(function (news_providers_count) {
                result.news_providers_count = news_providers_count;
                cb();
            }).failure(function (err) { cb(err); });
        },
        function (cb) {
            models.NewsItem.count().success(function (news_items_count) {
                result.news_items_count = news_items_count;
                cb();
            }).failure(function (err) { cb(err); });
        },
        function (cb) {
            models.ImportTask.count({where: {model: 'news_item', completed: false}}).success(function (incomplete_import_tasks_count) {
                result.incomplete_import_tasks_count = incomplete_import_tasks_count;
                cb();
            }).failure(function (err) { cb(err); });
        },
        function (cb) {
            models.ImportTask.findAll({where: {model: 'news_item', completed: false}}).success(function (incomplete_import_tasks) {
                result.incomplete_import_tasks = incomplete_import_tasks;
                cb();
            }).failure(function (err) { cb(err); });
        },
        function (cb) {
            models.ImportTask.find({where: {model: 'news_item'}, limit: 1, order: 'id desc'}).success(function (import_tasks) {
                result.latest_import_task = (import_tasks && import_tasks.dataValues) ? import_tasks.dataValues : null;
                cb();
            }).failure(function (err) { cb(err); });
        }
    ], function (err, cb) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.jsonp(result);
        }

    });
};