'use strict';
var request = require('request');
var models = require('../models');
var async = require('async');

var primary_host = "http://bitcoinchain.com";
var primary_url = primary_host + "/api/blocks";

var historic_mode = false;
console.log('historic_mode', historic_mode);
// historic mode: start with the oldest imported, and try to import even older ones, keep going to the first mined
// default mode: start with the youngest imported, and try to import even younger ones, keep going to the last mined

//https://bitcoinchain.com/api/blocks?blockDate=2014-11-02
/*
 model:
 height: 328252,
 size: 744475,
 hash: "0000000000000000058c8a2a9c3890764c46ac93adbf028b4a4ebe8c2c714722",
 time: "1414972729",
 txlength: 1044,
 poolInfo: {
 poolName: "Slush",
 url: "https://mining.bitcoin.cz/"
 } -> pool_name, pool_url
 */

models.sequelize.sync()
    .success(function () {
        var nextBlockDate = null;
        var active_request = false;

        function requestNextDay () {
            if (active_request) return;
            var url = primary_url + (nextBlockDate ? ('?blockDate=' + nextBlockDate) : '');
            active_request = true;
            request({
                    url: url,
                    json: true,
                    rejectUnauthorized: false},
                function (error, response, data) {
                    if (error || response.statusCode != 200) {
                        console.log('request error', error, response && response.statusCode);
                        clearInterval(handler);
                    } else {
                        if (!data.pagination || !data.pagination.prev || !data.length) {
                            console.log('probably got deep enough', nextBlockDate);
                            clearInterval(handler);
                        }
                        nextBlockDate = historic_mode ? data.pagination.prev : data.pagination.next;
                        //                for (var i=0; i < data.length; i++) {
                        //                    console.log(
                        //                        data.blocks[i].height,
                        //                        data.blocks[i].hash,
                        //                        data.blocks[i].poolInfo && data.blocks[i].poolInfo.poolName,
                        //                        data.blocks[i].time,
                        //                        data.blocks[i].txlength
                        //                    );
                        //                }
                        var inserted = 0;
                        var skipped = 0;

                        async.each(data.blocks, function (block, cb) {
                            if (block.poolInfo) {
                                block.pool_name = block.poolInfo.poolName;
                                block.pool_url = block.poolInfo.url;
                            }
                            block.time = new Date(block.time * 1000);
                            models.BlockHeader
                                .findOrCreate({where: {hash: block.hash}, defaults: block})
                                .success(function (ni, created) {
                                    if (created) inserted++; else  skipped++;
                                    cb();
                                });
                        }, function (err) {
                            if (err) {
                                console.log('error during async');
                                clearInterval(handler);
                                return;
                            }
                            console.log('for', nextBlockDate, 'got', data.length, 'inserted', inserted, 'skipped', skipped);
                            active_request = false;
                        });
                    }
                }
            );
        }

        // update nextBlockDate in case it's not the first start
        // note that format is YYYY-MM-DD
        var q = historic_mode ? models.BlockHeader.min('time') : models.BlockHeader.max('time');
        q.success(function (min_time) {
            if (min_time) {
                var month = min_time.getMonth() + 1;
                var day = min_time.getDate();
                nextBlockDate = min_time.getFullYear() + '-'
                    + ((month < 10) ? ('0' + month) : month) + '-'
                    + ((day < 10) ? ('0' + day) : day);
            }
            console.log('starting with', nextBlockDate);
            var handler = setInterval(requestNextDay, 500);
        });
        
    }).failure(function () {
        console.log('FAILURE WHILE CONNECTING/SYNCING TO DB', arguments);
    });