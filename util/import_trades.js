var import_trades = require('../import_tasks/trades');

//function deep_import(market, currency) {
//    console.log(market, currency, 'INIT');
//    var running = false;
//    var task_id = setInterval(function () {
//        if (!running) {
//            console.log(market, currency, 'NEXT RUN');
//            running = true;
//            import_trades(market, currency, function (err, import_task) {
//                running = false;
//                if (!import_task.imported_count) {
//                    console.log(market, currency, 'NOTHING MORE TO IMPORT HERE');
//                    clearInterval(task_id);
//                }
//            });
//        } else {
//            console.log(market, currency, 'INTERVAL, SKIPPED');
//        }
//    }, 3000);
//}

import_trades('btce', 'usd');
import_trades('btce', 'eur');
import_trades('bitstamp', 'usd');

import_trades('hitbtc', 'eur');
import_trades('hitbtc', 'usd');
