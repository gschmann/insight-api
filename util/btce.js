var api = require('../lib/BtceApi');
var program = require('commander');

var BTCE_VERSION = '0.1';

default_trades_args = { // TODO: ?
    'from': 0,
    'start_index': 0,
    'max_results': 10,
    'by': 'ts',
    'format_timestamp': 'second',
    'format_amount': 'number',
    'format_price': 'number',
    'format_wrap': false,
    'format_item': 'array'
};

program.version(BTCE_VERSION)
    .option('-t --trades', 'request trades')
    .option('-k --ticker', 'request ticker')
    .option('-o --orderbook', 'request orderbook')
    .option('-c --currency [value]', 'Currency usd/eur', 'usd')
    .parse(process.argv);

if (program.trades) {
    api.getTrades(program.currency, default_trades_args, function (err, trades) {
        if (err) {
            console.log('Error occurred', arguments);
        }
        console.log(trades);
    });
} else if (program.ticker) {
    api.getTicker(program.currency, function (err, trades) {
        if (err) {
            console.log('Error occurred', arguments);
        }
        console.log(trades);
    });
} else if (program.orderbook) {
    api.getOrderbook(program.currency, function (err, orderbook) {
        if (err) {
            console.log('Error occurred', arguments);
        }
        console.log(orderbook);
    });
} else {
    console.log('No action specified, use --help for usage details');
}