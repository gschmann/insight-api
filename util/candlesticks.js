'use strict';
// TODO: allow full rebuild and rebuild since date
// TODO: pass periods, markets and currencies as parameters

var db = require('../models');
var models = db;
var fs = require('fs');
var async = require('async');

var periods = [
    {alias: '20days', seconds: 20 * 60 * 60 * 24},
    {alias: '10days', seconds: 10 * 60 * 60 * 24},
    {alias: 'day', seconds: 60 * 60 * 24},
    {alias: '12hours', seconds: 60 * 60 * 12},
    {alias: '8hours', seconds: 60 * 60 * 8},
    {alias: '4hours', seconds: 60 * 60 * 4},
    {alias: 'hour', seconds: 60 * 60},
    {alias: '30minutes', seconds: 30 * 60},
    {alias: '15minutes', seconds: 15 * 60},
    {alias: '5minutes', seconds: 5 * 60},
    {alias: 'minute', seconds: 60}
];

var markets = [
    {name: 'hitbtc', currencies: ['usd', 'eur']},
    {name: 'btce', currencies: ['usd', 'eur']},
    {name: 'bitstamp', currencies: ['usd']}
];

function run_query(query_template, variables, callback) {
    for (var key in variables) {
        if (!variables.hasOwnProperty(key)) continue;
        query_template = query_template.replace(new RegExp(key, 'g'), variables[key]);
    }
    (function (market, currency, periodAlias) {
        db.sequelize.query(query_template)
            .success(function () {
                callback.apply(null, Array.prototype.slice.call(arguments, 0).concat([market, currency, periodAlias]));
            })
            .failure(function () {
                callback.apply(null, Array.prototype.slice.call(arguments, 0).concat([market, currency, periodAlias]));
            }
        );
    })(variables[':market'], variables[':currency'], variables[':periodAlias']);
}

fs.readFile(__dirname + '/candlestick.sql',
    function (err, data) {
        if (err) {
            throw err;
        }
        var candlestick_query_template = data.toString();
        var task_list = [];
        console.log('STARTING CANDLESTICK GENERATION');
        async.mapSeries(markets,
            function market_iterator (market, first_callback) {
                console.log('STARTING ', market.name);
                async.mapSeries(market.currencies,
                    function market_iterator (currency, mid_callback) {
                        console.log('STARTING ', market.name, currency);
                        async.mapSeries(periods,
                            function market_iterator (period, deepest_callback) {
                                console.log('\tSTARTED: ', market.name, currency, period.alias);
                                run_query(candlestick_query_template, {
                                    ':periodAlias': period.alias,
                                    ':periodSeconds': period.seconds,
                                    ':market': market.name,
                                    ':currency': currency
                                }, function (err, market, currency, periodAlias) {
                                    err = (err = []) ? false : err;
                                    console.log('\t', err ? 'FAILURE' : 'SUCCESS', market, currency, periodAlias);
                                    if (err) {
                                        logger.error(err);
                                    }
                                    deepest_callback();
                                });
                            },
                            function after_all_market_currency () {
                                console.log('All done for', market.name, currency);
                                mid_callback();
                            }
                        );
                    },
                    function after_all_market () {
                        console.log('All done for', market.name);
                        first_callback();
                    }
                );
            },
            function after_all () {
                console.log('all done');
            }
        );
    }
);