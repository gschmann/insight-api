-- The main idea of this candlestick building query is using PSQL WINDOW functional
-- See http://www.postgresql.org/docs/9.1/static/tutorial-window.html for basic information

INSERT INTO "Candlesticks"
  (
    time, period,
    price, open, close, min, max, volume, count, currency_volume,
    market, currency,
    "createdAt", "updatedAt" -- TODO: probably one timestamp is enough as we never really update existing candlesticks
  )
  (
    SELECT DISTINCT
      -- this expression functionally means "14:51:22" / "10 minutes" = 14:50:00 and gives the window's lower bound
      to_timestamp(floor(extract('epoch' from time) / ':periodSeconds') * ':periodSeconds') AT TIME ZONE 'UTC' as time,
      ':periodAlias' as period, -- TODO: it's probably better for performance to store periodSeconds instead of alias

      avg(price) OVER w,
      first_value(price) OVER w as open,
      last_value(price) OVER w  as close,
      min(price) OVER w,
      max(price) OVER w,
      sum(amount) OVER w as volume,
      count(amount) OVER w as volume,
      sum(price * amount) OVER w as currency_volume,

      market,
      currency,

      now(), now()

    FROM "Trades"

    WHERE
      --  don't calculate candlesticks which are already calculated (lower bound for trades selection)
      time > coalesce((
        SELECT
          to_timestamp(floor(extract('epoch' from max(time))) + :periodSeconds) AT TIME ZONE 'UTC'
        FROM "Candlesticks"
        WHERE
          market = ':market'
          AND currency = ':currency'
          AND period = ':periodAlias'
        ), to_timestamp(0))

      -- don't calculate candlesticks which time is yet to come (upper bound for trades selection)
      AND time <= (
        SELECT
          to_timestamp(floor(extract('epoch' from max(time)) / ':periodSeconds') * ':periodSeconds') AT TIME ZONE 'UTC'
        FROM "Trades"
        WHERE
          market = ':market'
          AND currency = ':currency'
      )

     AND market = ':market'
     AND currency = ':currency'

    WINDOW w AS (PARTITION BY floor(extract('epoch' from time) / ':periodSeconds'))

    ORDER BY time
  ) -- end of SELECT subquery
;