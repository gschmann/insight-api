var api = require('../lib/BitstampApi');
var program = require('commander');

var BITSTAMP_VERSION = '0.1';

default_trades_args = { // TODO: these are from HitBTC
    'from': 0,
    'start_index': 0,
    'max_results': 10,
    'by': 'ts',
    'format_timestamp': 'second',
    'format_amount': 'number',
    'format_price': 'number',
    'format_wrap': false,
    'format_item': 'array'
};

program.version(BITSTAMP_VERSION)
    .option('-t --trades', 'request trades')
    .option('-k --ticker', 'request ticker')
    .option('-o --orderbook', 'request orderbook')
    .parse(process.argv);

if (program.trades) {
    api.getTrades('usd', default_trades_args, function (err, trades) {
        if (err) {
            console.log('Error occurred', arguments);
        }
        console.log(trades);
    });
} else if (program.ticker) {
    api.getTicker('usd', function (err, trades) {
        if (err) {
            console.log('Error occurred', arguments);
        }
        console.log(trades);
    });
} else if (program.orderbook) {
    api.getOrderbook('usd', function (err, orderbook) {
        if (err) {
            console.log('Error occurred', arguments);
        }
        console.log(orderbook);
    });
} else {
    console.log('No action specified, use --help for usage details');
}