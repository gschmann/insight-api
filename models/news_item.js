"use strict";

module.exports = function(sequelize, DataTypes) {
    var NewsItem = sequelize.define("NewsItem", {
        title: DataTypes.STRING,
        summary: DataTypes.STRING,
        author: DataTypes.STRING,
        pubdate: DataTypes.DATE,
        link: DataTypes.STRING
    }, {
        classMethods: {
            associate: function(models) {
                NewsItem.belongsTo(models.NewsProvider)
            }
        }
    });

    return NewsItem;
};