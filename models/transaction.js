"use strict";

var SNAPSHOT_SIZE = 50;

module.exports = function(sequelize, DataTypes) {
    var Transaction = sequelize.define("Transaction", {
        txid: DataTypes.STRING(255),
        valueOut: DataTypes.FLOAT,
        size: DataTypes.BIGINT
    }, { classMethods: {
        updateSnapshot: function (models, new_tx, callback) {
            callback = callback || function () {};

            var sql = 'begin;' +
                'delete from "Transactions" where id in' +
                '(select id from "Transactions" order by "createdAt" desc offset :MAX);' +
                'insert into "Transactions" (txid, "valueOut", size, "createdAt", "updatedAt")' +
                "VALUES (':txid', ':valueOut', ':size', now(), now());" +
                "commit;";

            new_tx['MAX'] = SNAPSHOT_SIZE - 1;
            new_tx['size'] = new_tx['size'] || 0;

            // TODO: refactor (see util/candlesticks.js : run_query() for similar code)
            for (var key in new_tx) {
                if (!new_tx.hasOwnProperty(key)) continue;
                sql = sql.replace(new RegExp(':' + key, 'g'), new_tx[key]);
            }

//            console.log('ready to run', sql);

            models.sequelize.query(sql).success(callback).failure(callback);

        },
        getSnapshot: function () {
            return Transaction.findAll({order: '"createdAt" desc', limit: SNAPSHOT_SIZE});
        }
    }});

    return Transaction;
};