"use strict";
/*
 * See http://stockcharts.com/school/doku.php?id=chart_school:chart_analysis:introduction_to_candlesticks
 * to get familiar with the concept of candlesticks.
 *
 * `\d candlestick` output from original psql database:
 *       Column      |            Type             |                        Modifiers
 -----------------+-----------------------------+----------------------------------------------------------
     id              | integer                     | not null default nextval('candlestick_id_seq'::regclass)
     period          | character varying(10)       | not null
     open            | numeric(15,6)               | not null
     close           | numeric(15,6)               | not null
     min             | numeric(15,6)               | not null
     max             | numeric(15,6)               | not null
     volume          | double precision            | not null
     currency_volume | double precision            | not null
     market          | character varying(30)       | not null
     currency        | character(3)                | not null
     time            | timestamp without time zone | not null
     price           | double precision            |
     Indexes:
     "PRIMARY_CANDLESTICK" PRIMARY KEY, btree (id)
     "UNIQUE_CANDLESTICK" UNIQUE CONSTRAINT, btree (period, market, currency, "time")
     "candlestick_index01" btree (market, currency)

 // TODO: markets as a separate table (?)
 // TODO: currencies as a separate table (???)

 original period constants from PHP former implementation:
 CONST PERIOD_DAY = 'day';
 CONST PERIOD_FOUR_HOURS = '4hours';
 CONST PERIOD_HOUR = 'hour';
 CONST PERIOD_HALF_HOUR = '30minutes';
 CONST PERIOD_FIFTEEN_MINUTES = '15minutes';
 CONST PERIOD_FIVE_MINUTES = '5minutes';
 CONST PERIOD_MINUTE = 'minute';

 public static function periodToSeconds($period) {
     switch($period) {
         case self::PERIOD_DAY:
            return 60 * 60 * 24;
         case self::PERIOD_FOUR_HOURS:
            return 60 * 60 * 4;
         case self::PERIOD_HOUR:
            return 60 * 60;
         case self::PERIOD_HALF_HOUR:
            return 60 * 30;
         case self::PERIOD_FIFTEEN_MINUTES:
            return 60 * 15;
         case self::PERIOD_FIVE_MINUTES:
            return 60 * 5;
         case self::PERIOD_MINUTE:
            return 60;
         default:
            return 0;
     }
 }

 static $apiMarketsCurrencies = array(
     'btce' => array(
        'usd',
        'eur',
     ),
     'bitstamp' => array(
        'usd'
     ),
     'hitbtc' => array(
         'usd',
         'eur'
     ),
 );

 static $apiMarketsTradeUrls = array(
     'btce' => array(
         'usd' => 'https://btc-e.com/api/2/btc_usd/trades',
         'eur' => 'https://btc-e.com/api/2/btc_eur/trades',
//       'rub' => 'https://btc-e.com/api/2/btc_rur/trades'
     ),
     'bitstamp' => array(
        'usd' => 'https://www.bitstamp.net/api/transactions?time=hour'
     )
     // TODO: figure out trades for HitBTC
 );

 static $apiMarketsOrderbookUrls = array(
    'btce' => array(
         'usd' => 'https://btc-e.com/api/2/btc_usd/depth',
         'eur' => 'https://btc-e.com/api/2/btc_eur/depth',
 //      'rub' => 'https://btc-e.com/api/2/btc_rur/depth'
    ),
    'hitbtc' => array(
         'usd' => 'http://api.hitbtc.com/api/1/public/BTCUSD/orderbook',
         'eur' => 'http://api.hitbtc.com/api/1/public/BTCEUR/orderbook',
    ),
 'bitstamp' => array(
        'usd' => 'https://www.bitstamp.net/api/order_book/'
    )
 );

 static $apiMarketsTickerUrls = array(
 'btce' => array(
 'usd' => 'https://btc-e.com/api/2/btc_usd/ticker',
 'eur' => 'https://btc-e.com/api/2/btc_eur/ticker',
 //            'rub' => 'https://btc-e.com/api/2/btc_rur/ticker'
 ),
 'hitbtc' => array(
 'usd' => 'http://api.hitbtc.com/api/1/public/BTCUSD/ticker',
 'eur' => 'http://api.hitbtc.com/api/1/public/BTCEUR/ticker',
 ),
 'bitstamp' => array(
 'usd' => 'https://www.bitstamp.net/api/ticker/'
 )
 );

 public static function getList() {
 return array(
 'btce',
 'bitstamp',
 'hitbtc',
 );
 }
 */

module.exports = function(sequelize, DataTypes) {
    var Candlestick = sequelize.define("Candlestick", {
        period: DataTypes.STRING(10),
        open: DataTypes.DECIMAL(15, 6),
        close: DataTypes.DECIMAL(15, 6),
        min: DataTypes.DECIMAL(15, 6),
        max: DataTypes.DECIMAL(15, 6),
        volume: DataTypes.FLOAT,
        count: DataTypes.INTEGER,
        currency_volume: DataTypes.FLOAT,
        market: DataTypes.STRING(30),
        currency: DataTypes.STRING(3),
        time: DataTypes.DATE,
        price: DataTypes.FLOAT
    }, { classMethods: {
        // TODO: all retrieval and parsing operations here
    }});

    return Candlestick;
};