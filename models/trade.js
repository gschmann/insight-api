"use strict";
/**
 * original PSQL \d trade from former bitcoinchain.com for reference:
    id       | integer                     | not null default nextval('trade_id_seq'::regclass)
    market   | character varying(255)      | not null
    currency | character(3)                | not null
    price    | numeric(15,6)               | not null
    amount   | double precision            | not null
    time     | timestamp without time zone | not null
    tid      | bigint                      |
 */

module.exports = function(sequelize, DataTypes) {
    var Trade = sequelize.define("Trade", {
        market: DataTypes.STRING(255),
        currency: DataTypes.STRING(3),
        price: DataTypes.DECIMAL(15, 6),
        amount: DataTypes.FLOAT,
        time: DataTypes.DATE,
        tid: DataTypes.BIGINT
    }, { classMethods: {}});

    return Trade;
};