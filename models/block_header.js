"use strict";

module.exports = function(sequelize, DataTypes) {
    var BlockHeader = sequelize.define("BlockHeader", {
        hash: {type: DataTypes.STRING(64), unique: true},
        height: DataTypes.INTEGER,
        size: DataTypes.INTEGER,
        txlength: DataTypes.INTEGER,
        time: DataTypes.DATE,
        pool_name: DataTypes.STRING(64),
        pool_url: DataTypes.STRING(64)
    }, { classMethods: {
    }});

    return BlockHeader;
};