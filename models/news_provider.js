"use strict";

module.exports = function(sequelize, DataTypes) {
    var NewsProvider = sequelize.define("NewsProvider", {
        name: DataTypes.STRING,
        base_url: DataTypes.STRING,
        feed_url: DataTypes.STRING
    }, {
        classMethods: {
            associate: function(models) {
                NewsProvider.hasMany(models.NewsItem)
            }
        }
    });

    return NewsProvider;
};