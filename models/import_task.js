"use strict";

module.exports = function(sequelize, DataTypes) {
    var ImportTask = sequelize.define("ImportTask", {
        model: DataTypes.STRING,
        completed: { type: DataTypes.BOOLEAN, defaultValue: false },
        imported_count: { type: DataTypes.INTEGER, defaultValue: 0 },
        ignored_count: { type: DataTypes.INTEGER, defaultValue: 0 }
    }, {
        classMethods: {
        }
    });

    return ImportTask;
};