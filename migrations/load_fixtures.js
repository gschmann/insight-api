var sequelize_fixtures = require('sequelize-fixtures');
var models = require('../models');

module.exports = function (callback) { sequelize_fixtures.loadFile(__dirname + '/*__init.json', models, callback) };